package springmvc.favorite.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springmvc.currency.Currency;
import springmvc.favorite.Favorite;
import springmvc.favorite.dao.IFavoriteDao;

@Service
public class FavoriteServiseImp implements IFavoriteService {

	@Autowired
	IFavoriteDao favoriteDao;
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void addFavorite(Favorite favorite) {
		favoriteDao.addFavorite(favorite);

	}

	@Override
	public void removeFavorite(int favId) {

		favoriteDao.removeFavorite(favId);

	}

	@Override
	public Favorite getFavoriteById(long favId) {
		return favoriteDao.getFavoriteById(favId);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Currency> allFavorite(int userId) {
		List<Object[]> resultList = (List<Object[]>) entityManager
				.createQuery("select  c.id, c.name, c.symbol, c.slug," + " c.cmc_rank, c.price, c.volume_24h, "
						+ "c.percent_change_1h, c.percent_change_24h,"
						+ " c.percent_change_7d from Favorite v join Currency "
						+ "c on c.id = v.currency join User u on u.id = v.user where " + "u.id =:userId GROUP BY c.id")
				.setParameter("userId", userId).getResultList();

		return resultList.stream().map((Object[] o) -> {
			Currency currency = new Currency();
			currency.setId(Integer.parseInt(o[0].toString()));
			currency.setName(o[1].toString());
			currency.setSymbol(o[2].toString());
			currency.setSlug(o[3].toString());
			currency.setCmc_rank(Integer.parseInt(o[4].toString()));
			currency.setPrice(Double.parseDouble(o[5].toString()));
			currency.setVolume_24h(Double.parseDouble(o[6].toString()));
			currency.setPercent_change_1h(Double.parseDouble(o[7].toString()));
			currency.setPercent_change_24h(Double.parseDouble(o[8].toString()));
			currency.setPercent_change_7d(Double.parseDouble(o[9].toString()));

			return currency;
		}).collect(Collectors.toList());
	}

}
