package springmvc.favorite.service;

import java.util.List;

import springmvc.currency.Currency;
import springmvc.favorite.Favorite;

public interface IFavoriteService {
	void addFavorite(Favorite favorite);

	void removeFavorite(int favId);

	Favorite getFavoriteById(long favId);

	List<Currency> allFavorite(int userId);
}
