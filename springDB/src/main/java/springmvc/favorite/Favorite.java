package springmvc.favorite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import springmvc.currency.Currency;
import springmvc.security.model.User;

@Entity
@Table(name = "fav")
public class Favorite {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "fav_id")
	private long favId;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	// @ManyToOne
	@OneToOne
	@JoinColumn(name = "id")
	Currency currency;

	public Favorite() {
		super();
	}

	public Favorite(User user, Currency currency) {
		super();
		this.user = user;
		this.currency = currency;
	}

	public long getFavId() {
		return favId;
	}

	public void setFavId(long favId) {
		this.favId = favId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
