package springmvc.favorite.dao;

import java.util.List;

import springmvc.currency.Currency;
import springmvc.favorite.Favorite;

public interface IFavoriteDao {
	void addFavorite(Favorite favorite);

	//public void removeFavorite(long favId);
	public void removeFavorite(int favId);

	Favorite getFavoriteById(long favId);

}
