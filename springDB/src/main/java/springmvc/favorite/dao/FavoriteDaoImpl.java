package springmvc.favorite.dao;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import springmvc.currency.Currency;
import springmvc.currency.service.ICurrencyService;
import springmvc.favorite.Favorite;

@Transactional
@Repository
public class FavoriteDaoImpl implements IFavoriteDao {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void addFavorite(Favorite favorite) {
		if (favorite == null) {
			entityManager.persist(favorite);
		} else
			entityManager.merge(favorite);

	}

	@Override
	public void removeFavorite(int favId) {
		
		Currency item = entityManager.find(Currency.class, favId);
		entityManager.remove(item);

		/*
		 * Query query = entityManager
		 * .createQuery("delete Favorite from Favorite join  Currency c on c.id = Favorite.currency"
		 * + " join User u on u.id = Favorite.user where u.id=:userId and c.id=:id")
		 * .setParameter("userId", userId).setParameter("id", id);
		 * query.executeUpdate();
		 */

	}

	@Override
	public Favorite getFavoriteById(long favId) {
		return entityManager.find(Favorite.class, favId);

	}

}
