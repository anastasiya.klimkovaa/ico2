package springmvc.currency.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import springmvc.currency.Currency;

@Transactional
@Repository("CurrencyDAO")
public class CurrencyDaoImpl implements ICurrencyDao {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Currency> allCurrency() {

		return (List<Currency>) entityManager.createQuery("from Currency").getResultList();

	}

	@Override
	public Currency getCurrencyByID(int id) {

		return entityManager.find(Currency.class, id);
	}

	@Override
	public Currency getCurrencybyName(String name) {
		return (Currency) entityManager.createQuery("from Currency where name = :name").setParameter("name", name)
				.getSingleResult();
	}

}
