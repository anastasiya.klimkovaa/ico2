package springmvc.currency.dao;

import java.util.List;

import springmvc.currency.Currency;

public interface ICurrencyDao {
	List<Currency> allCurrency();

	Currency getCurrencyByID(int id);

	Currency getCurrencybyName(String name);

}
