package springmvc.currency;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import springmvc.favorite.Favorite;

@Entity
@Table(name = "Currencies")
public class Currency implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "id_firs")
	private int id_firs;
	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "symbol")
	private String symbol;
	@Column(name = "slug")
	private String slug;
	@Column(name = "cmc_rank")
	private int cmc_rank;
	@Column(name = "price")
	private double price;
	@Column(name = "volume_24h")
	private double volume_24h;
	@Column(name = "percent_change_1h")
	private double percent_change_1h;
	@Column(name = "percent_change_24h")
	private double percent_change_24h;
	@Column(name = "percent_change_7d")
	private double percent_change_7d;
	@Column(name = "createdAt")
	private Timestamp creatdAt;
	@Column(name = "updatedAt")
	private Timestamp updateAt;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "fav_id")
	Favorite favorite;

	public Favorite getFavorite() {
		return favorite;
	}

	public void setFavorite(Favorite favorite) {
		this.favorite = favorite;
	}

	public Currency() {
		super();
	}

	public Currency(int id_firs, int id, String name, String symbol, String slug, int cmc_rank, double price,
			double volume_24h, double percent_change_1h, double percent_change_24h, double percent_change_7d,
			Timestamp creatdAt, Timestamp updateAt) {
		super();
		this.id_firs = id_firs;
		this.id = id;
		this.name = name;
		this.symbol = symbol;
		this.slug = slug;
		this.cmc_rank = cmc_rank;
		this.price = price;
		this.volume_24h = volume_24h;
		this.percent_change_1h = percent_change_1h;
		this.percent_change_24h = percent_change_24h;
		this.percent_change_7d = percent_change_7d;
		this.creatdAt = creatdAt;
		this.updateAt = updateAt;
	}

	public int getId_firs() {
		return id_firs;
	}

	public void setId_firs(int id_firs) {
		this.id_firs = id_firs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public int getCmc_rank() {
		return cmc_rank;
	}

	public void setCmc_rank(int cmc_rank) {
		this.cmc_rank = cmc_rank;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getVolume_24h() {
		return volume_24h;
	}

	public void setVolume_24h(double volume_24h) {
		this.volume_24h = volume_24h;
	}

	public double getPercent_change_1h() {
		return percent_change_1h;
	}

	public void setPercent_change_1h(double percent_change_1h) {
		this.percent_change_1h = percent_change_1h;
	}

	public double getPercent_change_24h() {
		return percent_change_24h;
	}

	public void setPercent_change_24h(double percent_change_24h) {
		this.percent_change_24h = percent_change_24h;
	}

	public double getPercent_change_7d() {
		return percent_change_7d;
	}

	public void setPercent_change_7d(double percent_change_7d) {
		this.percent_change_7d = percent_change_7d;
	}

	public Timestamp getCreatdAt() {
		return creatdAt;
	}

	public void setCreatdAt(Timestamp creatdAt) {
		this.creatdAt = creatdAt;
	}

	public Timestamp getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	@Override
	public String toString() {
		return "MyCurrency [id_firs=" + id_firs + ", id=" + id + ", name=" + name + ", symbol=" + symbol + ", slug="
				+ slug + ", cmc_rank=" + cmc_rank + ", price=" + price + ", volume_24h=" + volume_24h
				+ ", percent_change_1h=" + percent_change_1h + ", percent_change_24h=" + percent_change_24h
				+ ", percent_change_7d=" + percent_change_7d + ", creatdAt=" + creatdAt + ", updateAt=" + updateAt
				+ "]";
	}

	public static Currency create(int id_firs, int id, String name, String symbol, String slug, int cmc_rank,
			double price, double volume_24h, double percent_change_1h, double percent_change_24h,
			double percent_change_7d, Timestamp creatdAt, Timestamp updateAt) {
		Currency currency = new Currency();
		currency.setId_firs(id_firs);
		currency.setId(id);
		currency.setName(name);
		currency.setSymbol(symbol);
		currency.setSlug(slug);
		currency.setCmc_rank(cmc_rank);
		currency.setPrice(price);
		currency.setVolume_24h(volume_24h);
		currency.setPercent_change_1h(percent_change_1h);
		currency.setPercent_change_24h(percent_change_24h);
		currency.setPercent_change_7d(percent_change_7d);
		currency.setCreatdAt(creatdAt);
		currency.setUpdateAt(updateAt);

		return currency;
	}

}
