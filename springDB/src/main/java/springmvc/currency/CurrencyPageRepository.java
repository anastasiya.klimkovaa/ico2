package springmvc.currency;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CurrencyPageRepository extends PagingAndSortingRepository<Currency,Integer> {
	
	
}
