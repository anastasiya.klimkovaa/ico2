package springmvc.currency.service;

import java.util.List;

import springmvc.currency.Currency;

public interface ICurrencyService {
	
	List<Currency> allCurrency();
	Currency getCurrencyByID(int id);
	Currency getCurrencybyName(String name);
	Currency saveFavorite(Currency myCurrency);
	
}
