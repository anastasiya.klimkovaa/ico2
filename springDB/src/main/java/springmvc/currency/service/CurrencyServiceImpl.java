package springmvc.currency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springmvc.currency.CurrencyRepository;
import springmvc.currency.Currency;
import springmvc.currency.dao.ICurrencyDao;
import springmvc.favorite.Favorite;


@Service
public class CurrencyServiceImpl implements ICurrencyService {
	@Autowired
	ICurrencyDao currencyDAO;
	
	private CurrencyRepository currencyRepository;
	
	@Override
	public List<Currency> allCurrency() {
		
		return currencyDAO.allCurrency();
	}

	@Override
	public Currency getCurrencyByID(int id) {
		Currency currencyId = currencyDAO.getCurrencyByID(id);
		return currencyId;
	}

	@Override
	public Currency getCurrencybyName(String name) {
		Currency currencyName= currencyDAO.getCurrencybyName(name);
		return currencyName;
	}

	@Override
	public Currency saveFavorite(Currency myCurrency) {
		Favorite fav = new Favorite();
		myCurrency.setFavorite(fav);
		fav.setCurrency(myCurrency);
		
		return currencyRepository.save(myCurrency);
	}

}
