package springmvc.news.service;

import java.util.List;

import springmvc.news.News;

public interface INewsService {
	List<News> allNews();

	List<News> getNewsbyCategory(String category);

}
