package springmvc.news.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springmvc.news.News;
import springmvc.news.dao.INewsDao;
@Service
public class NewsServiceImpl implements INewsService {
	@Autowired
	INewsDao newsDao;

	@Override
	public List<News> allNews() {
		return newsDao.allNews();
	}

	@Override
	public List<News> getNewsbyCategory(String category) {
		
		return newsDao.getNewsbyCategory(category);
	}

}
