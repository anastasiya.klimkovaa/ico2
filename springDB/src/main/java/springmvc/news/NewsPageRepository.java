package springmvc.news;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface NewsPageRepository extends PagingAndSortingRepository<News, String>{

}
