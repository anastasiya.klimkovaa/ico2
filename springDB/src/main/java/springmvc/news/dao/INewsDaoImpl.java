package springmvc.news.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import springmvc.news.News;

@Transactional
@Repository
public class INewsDaoImpl implements INewsDao {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<News> allNews() {

		return (List<News>) entityManager.createQuery("from News").getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> getNewsbyCategory(String category) {
		return (List<News>) entityManager.createQuery("from News where category = :category")
				.setParameter("category", category).getResultList();

	}

}
