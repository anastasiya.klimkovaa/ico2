package springmvc.news.dao;

import java.util.List;
import springmvc.news.News;

public interface INewsDao {
	List<News> allNews();

	List<News> getNewsbyCategory(String category);

}
