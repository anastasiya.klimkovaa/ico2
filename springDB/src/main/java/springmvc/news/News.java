package springmvc.news;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "News")
public class News {
	@Column(name = "id_firs")
	private int id_firs;
	@Id
	@Column(name = "id")
	private String id;
	@Column(name = "description")
	private String description;
	@Column(name = "soursUrl")
	private String soursUrl;
	@Column(name = "name")
	private String name;
	@Column(name = "category")
	private String category;
	@Column(name = "title")
	private String title;
	@Column(name = "url")
	private String url;
	@Column(name = "publishedAt")
	private Timestamp publishedAt;
	@Column(name = "originalImageUrl")
	private String originalImageUrl;
	@Column(name = "createdAt")
	private Timestamp creatdAt;
	@Column(name = "updatedAt")
	private Timestamp updateAt;
	
	
	public int getId_firs() {
		return id_firs;
	}
	public void setId_firs(int id_firs) {
		this.id_firs = id_firs;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSoursUrl() {
		return soursUrl;
	}
	public void setSoursUrl(String soursUrl) {
		this.soursUrl = soursUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Timestamp getPublishedAt() {
		return publishedAt;
	}
	public void setPublishedAt(Timestamp publishedAt) {
		this.publishedAt = publishedAt;
	}
	public String getOriginalImageUrl() {
		return originalImageUrl;
	}
	public void setOriginalImageUrl(String originalImageUrl) {
		this.originalImageUrl = originalImageUrl;
	}
	public Timestamp getCreatdAt() {
		return creatdAt;
	}
	public void setCreatdAt(Timestamp creatdAt) {
		this.creatdAt = creatdAt;
	}
	public Timestamp getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}
	
	
	

}
