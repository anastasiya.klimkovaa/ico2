package springmvc.security.service;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import springmvc.favorite.Favorite;

import springmvc.security.model.Role;
import springmvc.security.model.User;
import springmvc.security.repository.RoleRepository;
import springmvc.security.repository.UserRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
@Service("userService")
public class UserService {

	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public UserService(UserRepository userRepository, RoleRepository roleRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public User saveUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setActive(1);
		Role userRole = roleRepository.findByRole("USER");
		if (userRole == null) {
			userRole = roleRepository.save(new Role("USER"));
		}

		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

		Favorite fav = new Favorite();
		user.setFavorite(fav);
		fav.setUser(user);
		return userRepository.save(user);
	}
	 public User findByEmail(String email){
	        return userRepository.findByEmail(email);
	    }
	    public void updatePassword(String password, int i) {
	        userRepository.updatePassword(password, i);
	    }

		
	    

}
