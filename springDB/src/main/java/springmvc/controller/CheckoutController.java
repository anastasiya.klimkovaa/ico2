package springmvc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import springmvc.card.ChargeRequest;

@Controller
public class CheckoutController {
 
   
    private String stripePublicKey ="pk_test_IPuM3RMDdiKj4JuCiA2fTF0W00t46rAUgM";
 
    @RequestMapping("/checkout")
    public String checkout(Model model) {
        model.addAttribute("amount", 50 * 10); 
        model.addAttribute("stripePublicKey", stripePublicKey);
        model.addAttribute("currency", ChargeRequest.Currency.USD);
       return "checkout";
       
    }
}