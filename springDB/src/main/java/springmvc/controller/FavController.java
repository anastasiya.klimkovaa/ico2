package springmvc.controller;

import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseStatus;

import springmvc.currency.Currency;
import springmvc.currency.service.ICurrencyService;
import springmvc.favorite.Favorite;

import springmvc.favorite.service.IFavoriteService;
import springmvc.security.model.User;
import springmvc.security.service.UserService;

@Controller
public class FavController {

	@Autowired
	private UserService userService;
	@Autowired
	private ICurrencyService currencyServise;
	@Autowired(required = true)
	private IFavoriteService favoriteServise;

	@GetMapping("/user/currency/add/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Currency> getCurById(@PathVariable("id") Integer id) {

		{
			org.springframework.security.core.userdetails.User user = 
					(org.springframework.security.core.userdetails.User) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();

			String emailId = user.getUsername();

			User usr = userService.findUserByEmail(emailId);
			Currency currency = currencyServise.getCurrencyByID(id);

			Favorite favorite = new Favorite();
			favorite.setUser(usr);
			favorite.setCurrency(currency);
			favoriteServise.addFavorite(favorite);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Location", "/user/currency/all");

			return new ResponseEntity<Currency>(currency, headers, HttpStatus.FOUND);
		}

	}


	@RequestMapping("/user/currency/delete/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public  HttpHeaders removeCurrencyItem(@PathVariable(value = "id") int id) {
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		String emailId = user.getUsername();
		favoriteServise.removeFavorite(id);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/user/currency/all");

		return headers;

	}

	@RequestMapping("/user/currency/all")
	public String indexList(Model model) {
		BasicConfigurator.configure();
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		String emailId = user.getUsername();

		User usr = userService.findUserByEmail(emailId);
		int userId = usr.getId();
		Favorite favorite = new Favorite();
		favorite.setUser(usr);

		List<Currency> currency = favoriteServise.allFavorite(userId);

		model.addAttribute("currency", currency);

		return "favorite";

	}

}
