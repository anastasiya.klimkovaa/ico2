package springmvc.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import springmvc.currency.Currency;
import springmvc.currency.CurrencyPageRepository;
import springmvc.currency.CurrencyRepository;
import springmvc.currency.PageModel;
import springmvc.currency.service.ICurrencyService;
import springmvc.news.News;
import springmvc.news.NewsPageRepository;
import springmvc.news.service.INewsService;
import springmvc.security.model.User;
import springmvc.security.service.UserService;

@Controller

public class TestController {
	@Autowired
	private ICurrencyService currencyServise;
	@Autowired
	private UserService userService;
	@Autowired
	INewsService newsService;
	@Autowired
	CurrencyPageRepository currencyPageRepositor;
	@Autowired
	NewsPageRepository newsPageRepository;
	@Value("${welcome.message:test}")
	private String message = "Hello World";

	private static final int BUTTONS_TO_SHOW = 3;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 7;
	private static final int[] PAGE_SIZES = { 10, 20 };
	@Autowired
	CurrencyPageRepository currencyPageRepository;

	@RequestMapping("/li")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		return "welcome";
	}

	@RequestMapping(path = "/example", method = RequestMethod.GET)
	public List<Currency> getAllEmployees() {
		return currencyServise.allCurrency();
	}

	@RequestMapping(value = { "", "/", "/home" }, method = RequestMethod.GET)
	public String homeController(Model model, @SortDefault("id") Pageable pageable) {
		Page<Currency> page = currencyPageRepositor.findAll(pageable);

		model.addAttribute("page", page);

		return "home";
	}


	

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String helloList(Model model, @SortDefault("id") Pageable pageable) {
		Page<Currency> page = currencyPageRepositor.findAll(pageable);

		model.addAttribute("page", page);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		model.addAttribute("userName",
				"Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");

		return "hello";
	}

	@RequestMapping(value = "/exm", method = RequestMethod.GET)
	public String exampleDialect(Model model, @SortDefault("id") Pageable pageable) {
		Page<Currency> page = currencyPageRepositor.findAll(pageable);

		model.addAttribute("page", page);

		return "exm";
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String news(Model model, @SortDefault("title") Pageable pageable) {
		Page<News> page = newsPageRepository.findAll(pageable);
		model.addAttribute("page", page);
		return "news";
	}

	@GetMapping("/some/{id}")
	public ResponseEntity<Currency> getArticleById(@PathVariable("id") Integer id) {
		Currency currency = currencyServise.getCurrencyByID(id);
		return new ResponseEntity<Currency>(currency, HttpStatus.OK);
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration() {
		ModelAndView modelAndView = new ModelAndView("");
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView("");
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user",
					"There is already a user registered with the email provided");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");

		}
		return modelAndView;
	}
	
	/*
	 * @SuppressWarnings("deprecation")
	 * 
	 * @GetMapping(value = { "", "/", "/home" }) public ModelAndView
	 * homepage(@RequestParam("pageSize") Optional<Integer> pageSize,
	 * 
	 * @RequestParam("page") Optional<Integer> page){
	 * 
	 * ModelAndView modelAndView = new ModelAndView("home"); int evalPageSize =
	 * pageSize.orElse(INITIAL_PAGE_SIZE); int evalPage = (page.orElse(0) < 1) ?
	 * INITIAL_PAGE : page.get() - 1; System.out.println("here is client repo " +
	 * currencyPageRepository.findAll()); Page<Currency> clientlist =
	 * currencyPageRepository.findAll(new PageRequest(evalPage, evalPageSize));
	 * System.out.println("client list get total pages" + clientlist.getTotalPages()
	 * + "client list get number " + clientlist.getNumber()); PageModel pager = new
	 * PageModel(clientlist.getTotalPages(),clientlist.getNumber(),BUTTONS_TO_SHOW);
	 * 
	 * modelAndView.addObject("clientlist",clientlist);
	 * 
	 * modelAndView.addObject("selectedPageSize", evalPageSize);
	 * 
	 * modelAndView.addObject("pageSizes", PAGE_SIZES);
	 * 
	 * modelAndView.addObject("pager", pager); return modelAndView;
	 * 
	 * }
	 */

}
